public class buscar_impresora {
 /* */
    public static void main(String[] args) throws Exception {
        PrintService myPrintService = findPrintService("EPSON L220 Series");

    }

    private static PrintService findPrintService(String printerName) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        for (PrintService printService : printServices) {
            System.out.println("" + printService.getName());
            if (printService.getName().trim().equals(printerName)) {
                return printService;
            }
        }
        return null;
    }
}